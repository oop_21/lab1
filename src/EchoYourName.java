import java.util.Scanner;

public class EchoYourName {
    public static void main(String [] arsg) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input your name: ");
        String name = sc.next();
        System.out.println("Hello " + name);
    }
}
